<?php

use Illuminate\Support\Facades\Route;
use App\Models\Post;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/mad','App\Http\Controllers\MadController@madhu');
Route::get('/list','App\Http\Controllers\MadController@list');
// Route::view('/list','list');
Route::post('/list','App\Http\Controllers\MadController@save');
// Route::get('/submit','App\Http\Controllers\MadController@submit');
Route::get('/delete/{id}','App\Http\Controllers\MadController@delete');
Route::get('/edit/{id}','App\Http\Controllers\MadController@edit');
Route::get('/update/{id}','App\Http\Controllers\MadController@update');

Route::resource('editor','App\Http\Controllers\CKEditorController');

Route::post('editor/image_upload', 'App\Http\Controllers\CKEditorController@upload')->name('upload');

Route::resource('/posts','App\Http\Controllers\PostController');



