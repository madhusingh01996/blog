<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_categories', function (Blueprint $table) {
            $table->id();
            $table->unsignedinteger('user_id');
            // $table->bigIncrements('user_category_id');
            $table->string('author')->nullable();
            $table->string('admin')->nullable();
            $table->string('visitor')->nullable();
            $table->string('new_member')->timestamps();
            $table->string('member')->timestamps();
            // $table->string('blogger');
            // $table->string('editor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_categories');
    }
}
