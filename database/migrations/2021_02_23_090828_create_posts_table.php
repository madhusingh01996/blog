<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            // $table->unsignedinteger('post_category_id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('body');
            $table->string('section_title');
            $table->string('image');
            // $table->unsignedinteger('user_id');
            // $table->string('image')->nullable();
            // $table->enum('status',['Published','Draft'])->default('Draft');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
