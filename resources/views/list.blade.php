<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
</head>
<body>
	<h1>post page</h1>
	<div class="container">
		<table class="table" border="1">
			<thead class="table-dark">
				<tr>
					<th>id</th>
					<th>title</th>
					<th>slug</th>
					<th>body</th>
					<th>section_title</th>
					<th>image</th>
					<th>created_at</th>
					<th>updated_at</th>
					<th>action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($result as $list)
				<tr>
					<td>{{$list->id}}</td>
					<td>{{$list->title}}</td>
					<td>{{$list->slug}}</td>
					<td>{{$list->body}}</td>
					<td>{{$list->section_title}}</td>
					<td><img src="{{asset('storage/post/'.$list->image)}}" width="100px" /></td>
					<td>{{$list->created_at}}</td>
					<td>{{$list->updated_at}}</td>
					<td><a class="btn btn-info" style="color: #fff"  href="/edit/{{$list->id}}">Edit</a>
						<a class="btn btn-danger" href="/delete/{{$list->id}}">Delete</a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<div class="container">
		<form action="/list" enctype="multipart/form-data" method="POST">
			@csrf
			<div class="form-group">
			   	<label for="title">Title</label>
			   	<input class="form-control" type="text" name="title" value="{{ old('title') }}">
			</div>
			<div class="form-group">
			   	<label for="slug">Slug</label>
			   	<input class="form-control" type="text" name="slug" value="{{ old('slug') }}">
			</div>
			<div class="form-group">
				<label for="body">Body</label>
				<textarea class="form-control" name="body" rows="3"></textarea>
			</div>
			<div class="form-group">
				<label for="section_title">section title</label>
				<textarea class="form-control" name="section_title" rows="3"></textarea>
			</div>
			<div class="form-group">
				<label for="image">Image</label>
				<input type="file" name="image">
			</div>
			<button class="btn btn-primary" type="submit">Submit</button>
		</form>
	</div>
</body>
</html>