<textarea class="form-control" id="summary-ckeditor" name="summary-ckeditor"></textarea>

<!-- <script src="{{ asset('ckeditor/ckeditor.js') }}"></script> -->
<!-- <script>
CKEDITOR.replace( 'summary-ckeditor' );
</script> -->

<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<!-- <script>
CKEDITOR.replace( 'summary-ckeditor' );
</script> -->

<script>
CKEDITOR.replace( 'summary-ckeditor', {
    filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
    filebrowserUploadMethod: 'form'
});
</script>