<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Post;
use Validator;
class MadController extends Controller
{
    public function madhu(){
    	return view('layout');
    }
    public function list(){
    	$data['result']=DB::table('posts')->get();
    	// dd($data['result']);
    	return view('list',$data);
    }
    public function submit(Request $request){
    	// echo "hello";
    	$request->validate([
    		'title'=>'required',
            'slug'=>'required',
    		'body'=>'required',
    		'section_title'=>'required',
    		'image'=>'required',
    	]);

        // $data=array(
        // 'title'=>$request->input('title'),
        // 'slug'=>$request->input('slug'),
        // 'body'=>$request->input('body'),
        // 'section_title'=>$request->input('section_title'),
        // 'image'=>$request->file('image')->store('public')
        // );
        // DB::table('posts')->insert($data);
        // $request->session()->flash('msg','Data Saved');
        // return redirect('/list');
    }

    public function save(Request $req){
    	$post = new Post;
    	$post->title=$req->title;
        $post->slug=$req->slug;
    	$post->body=$req->body;
    	$post->section_title=$req->section_title;
    	$post->image=$req->file('image')->store('public');
        // $post->image=$req->file('image');
        // $exe=$post->$image->extension();
        // $file=time().'.'.$exe;
        // $post->$image->storeAs('/public/post',$file);
    	$post->save();
    }

    public function delete(Request $request,$id){
        // echo "delete";
        DB::table('posts')->where('id',$id)->delete();
        $request->session()->flash('msg','Data Deleted');
        return redirect('list');
    }

    public function edit(Request $request,$id){
        $data['result']=DB::table('posts')->where('id',$id)->get();
        // dd($data['result']);
        return view('edit',$data);
    }

    public function update(Request $request,$id){
        $post = new Post;
        $post->title=$req->title;
        $post->slug=$req->slug;
        $post->body=$req->body;
        $post->section_title=$req->section_title;
        $post->image=$req->file('image')->store('public');
        $post->save();

        DB::table('posts')->where('id',$id)->update($data);
        $request->session()->flash('msg','Data updated');
        return redirect('list');
    }

    // public function store(Request $request){
    // $validatedData = $this->validate($request, [
    //     'title'         => 'required|min:3|max:255',
    //     'slug'          => 'required|min:3|max:255|unique:posts',
    //     'description'   => 'required|min:3'
    // ]);

    // $validatedData['slug'] = Str::slug($validatedData['slug'], '-');

    // Post::create($validatedData);

    // return redirect()->route('post.index');
    // }


}
