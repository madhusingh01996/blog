<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $table = 'posts';
    // protected $fillable = ['title','content','post_category_id','user_id'];

    // public function category(){
    // 	return $this->belongsTo('App\Models\PostCategory');
    // }

    // public function author(){
    // 	return $this->belongsTo('App\Models\User');
    // }

    // public function comments(){
    // 	return $this->hasMany('App\Models\Comment','post_id');
    // }
     protected $fillable = ['title', 'slug','body','section_title','image'];
   
    /**
     * The has Many Relationship
     *
     * @var array
     */
    public function comments()
    {
        return $this->hasMany('App\Models\Comment')->whereNull('parent_id');
    }
}
